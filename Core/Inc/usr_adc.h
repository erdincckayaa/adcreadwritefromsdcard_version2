#ifndef __USR_ADC_H
#define __USR_ADC_H

#define _ADC_UNIT hadc1
#define _ADC_CHANNEL_COUNT 2
#define _ADC_SAMPLE_COUNT 8
#define _ADC_RESOULUTION 4095

#define _ADC_READING_TIMEOUT 100

#define _VREF_ADD (uint16_t *)0x1FFF7A2A ///// @ 3.V

void UsrAdcTimer(void);
void UsrAdcInitial(void);
EBool UsrAdcRead(void);
void UsrAdcReadingCallback(void);

extern float g_adcDataBuf;
extern float m_rawBuf[_ADC_CHANNEL_COUNT];
#endif //__USR_ADC_H

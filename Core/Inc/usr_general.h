#ifndef __USR_GENERAL_H
#define __USR_GENERAL_H

#include "main.h"
#include "usart.h"
#include "gpio.h"
#include "tim.h"
#include "adc.h"


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

/* generic defines*/
#define _io static
#define _iov static volatile

/*gets length of buffer*/
#define _size(x) strlen((const char *)x)

/* generic bool enum */
typedef enum
{
    false,
    true
} EBool;

/* generic converter union define */
typedef union U_CONVERTER_TAG
{
    uint8_t buf[4];
    uint32_t ui32val;
    int32_t i32val;
    uint16_t ui16val;
    int16_t i16val;
} U_CONVERTER;

typedef enum
{
    ENUM,
    STRING
} ECommand;

/*we include all the headers that we create */
#include "usr_system.h"
#include "usr_adc.h"
#endif //__USR_GENERAL_H

#ifndef __USR_SYSTEM_H
#define __USR_SYSTEM_H

#define _USR_SYSTEM_LOG_UART_TRANSMIT_CHANNEL huart4
#define _USR_SYSTEM_ADC_LOG_UART_TRANSMIT_CHANNEL huart2
#define _USR_SYSTEM_LOG_UART_BASE_RAW_CHANNEL_ADDR USART2



/*Led ON/OFF defines*/
#define _SYSTEM_LOG_ERROR_LED_Pin GPIO_PIN_12
#define _SYSTEM_LOG_ERROR_LED_GPIO_Port GPIOD
#define _SYSTEM_LOG_ERROR_TOGGLE_LED() (_SYSTEM_LOG_ERROR_LED_GPIO_Port->ODR ^= _SYSTEM_LOG_ERROR_LED_Pin)


// #define _SYSTEM_STATUS_LED(x) (x ? (_SYSTEM_STATUS_LED_GPIO_Port->BSRR = _SYSTEM_STATUS_LED_Pin) : (_SYSTEM_STATUS_LED_GPIO_Port->BRR = _SYSTEM_STATUS_LED_Pin))
// #define _STATUS_DATA_LED(x) (x ? (_DATA_PROCCESS_LED_GPIO_Port->BSRR = _DATA_PROCCESS_LED_Pin) : (_DATA_PROCCESS_LED_GPIO_Port->BRR = _DATA_PROCCESS_LED_Pin))

/* Led toggle pins to see some system tasks whether it works */
#define _SYSTEM_STATUS_TOGGLE_LED() (_SYSTEM_STATUS_LED_GPIO_Port->ODR ^= _SYSTEM_STATUS_LED_Pin)
#define _SYSTEM_DATA_TOGGLE_LED() (_DATA_PROCCESS_LED_GPIO_Port->ODR ^= _DATA_PROCCESS_LED_Pin)

void UsrAdcReadingCallback(void);
EBool UsrAdcRead(void);
EBool UsrSdCardInitial(void);

void UsrSystemTestInitial(void);
void UsrSystemTestGeneral(void);
void UsrTimersInitial(void);
void UsrErrorLedBlink(void);


extern volatile uint8_t g_conversionFlag;
extern uint32_t g_testTimerCnt;
extern volatile uint8_t g_adcReadOk;
extern float filter_out3;
extern void UsrgetAdcValue(void);
void LogWriteProc(void);
void printFiltredValuesProc(void);





#endif //__USR_SYSTEM_H

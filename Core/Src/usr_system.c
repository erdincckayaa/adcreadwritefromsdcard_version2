
#include "usr_general.h"

/* User variable decleration */

/* =======================================================================================================================*/

double m_AdcValue;

uint32_t g_testTimerCnt = 0;
uint8_t g_adcFinishFlag = false;

// 1. Filtre için ara değerler
double a_vn = 0.;
double a_vn1 = 0.;
double a_vn2 = 0.;
// 2. Filtre için ara değerler
double b_vn = 0.;
double b_vn1 = 0.;
double b_vn2 = 0.;
// 3. Filtre için ara değerler
double c_vn = 0.;
double c_vn1 = 0.;
double c_vn2 = 0.;
// 4. Filtre için ara değerler
double d_vn = 0.;
double d_vn1 = 0.;
double d_vn2 = 0.;

#define _ITERATION_SIZE 100000
// 1. Filtre çıkışı
double y = 0;
double filter_out = 0;
double filter_out2 = 0;
float filter_out3 = 0;
// 2. Filtre çıkışı

float m_sumAdcValue = 0;
// 2. Dereceden Filtre Katsayıları
// double b0 = 1.;
// double b1 = 0.;
// double b2 = -1.;
// double a1 = -1.98323277169363665;
// double a2 = 0.998995195336138008;
// double K = 0.000502402331931055766;
double b0 = 1;
double b1 = 0;
double b2 = -1;
double a1 = -1.979261232871670106447936632321216166019;
double a2 = 0.994986042646556656521283912297803908587;
double K  = 0.002506978676721646152186773193193403131;

// uint16_t counterBuf[_ITERATION_SIZE] = {0};
// float adcBuf[_ITERATION_SIZE];
// char tempBuf[_ITERATION_SIZE];
#define LOW_LIMIT 20000
#define HIGH_LIMIT 80000

uint32_t cnt1 = 0;

//#define SAMPLE_TIMER_UNIT 784

void UsrSystemTestInitial(void)
{
    /* ADC interrupt start */
    HAL_ADC_Start_IT(&hadc1);

    /* Timer intials*/
    UsrTimersInitial();
}

#define _ADC_TRIGGER_TIMER &htim2


void UsrTimersInitial(void)
{

    HAL_TIM_Base_Start(_ADC_TRIGGER_TIMER);
}

void UsrSystemTestGeneral(void)
{
    if (g_conversionFlag)
    {
        g_conversionFlag = false;
        UsrgetAdcValue();
        //printf("%f*%f\n", m_AdcValue, filter_out2);
        
    }
}

int i = 0;
double m_rawAdcValue = 0;
volatile uint8_t g_adcReadOk = false;
void UsrgetAdcValue(void)
{
    _SYSTEM_DATA_TOGGLE_LED();
    // if (i == 0)                                            // ilk basladigi anda adc start degerini baslat
    //     printf("ADC Read Start %d(mS)\n", g_testTimerCnt); // g_testTimerCnt increases in 1ms

    m_rawAdcValue = HAL_ADC_GetValue(&hadc1);

    m_AdcValue = ((3.3 * m_rawAdcValue) / 4095); // getting every ADC read and giving this value for filter signal

    /* filtering process */
    a_vn = m_AdcValue - a1 * a_vn1 - a2 * a_vn2;
    filter_out = K * (b0 * a_vn + b1 * a_vn1 + b2 * a_vn2);
    a_vn2 = a_vn1;
    a_vn1 = a_vn;

    b_vn = filter_out - a1 * b_vn1 - a2 * b_vn2;
    y = K * (b0 * b_vn + b1 * b_vn1 + b2 * b_vn2);
    b_vn2 = b_vn1;
    b_vn1 = b_vn;

    /* additional filter */
    c_vn = y - a1 * c_vn1 - a2 * c_vn2;
    filter_out2 = K * (b0 * c_vn + b1 * c_vn1 + b2 * c_vn2);
    c_vn2 = c_vn1;
    c_vn1 = c_vn;

    // d_vn = filter_out2 - a1 * d_vn1 - a2 * d_vn2;
    // filter_out3 = K * (b0 * c_vn + b1 * d_vn1 + b2 * d_vn2);
    // d_vn2 = d_vn1;
    // d_vn1 = d_vn;

    if (LOW_LIMIT < i && i < HIGH_LIMIT)
      {
        m_sumAdcValue += (filter_out2 * filter_out2);
    }

    /* check sample count */
    if (i++ >= _ITERATION_SIZE)
    {
        // printf("ADC Read Stop %d(mS)\n", g_testTimerCnt); // stop value
        float rmsValue = sqrt(m_sumAdcValue / (HIGH_LIMIT - LOW_LIMIT));
        HAL_TIM_Base_Stop(&htim2);

        printf("{\"inputResistor\":%f}\n", rmsValue);
        // printf("System goes RESET to get Ready for ADC READ in 5 sec...\n\n$$$");
        i = 0;
        m_sumAdcValue = 0;
        HAL_NVIC_SystemReset();
    }
}

/**************************************************************************************************************************/
/* @definition : when an error occured blink 12th pin of GPIOD
 *  @param : none
 *  @ret : none
 */
void UsrErrorLedBlink(void)
{
    for (int i = 0; i < 7; ++i)
    {
        _SYSTEM_LOG_ERROR_TOGGLE_LED();
        HAL_Delay(40);
    }
}
